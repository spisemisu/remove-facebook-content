#!/usr/bin/env stack
{- stack
   --resolver lts-11.5
   --install-ghc
   runghc
   --package aeson
   --package cmdargs
   --package http-client
   --package http-client-tls
   --package time
   --
   -Wall -Werror
-}

--------------------------------------------------------------------------------

-- {-# LANGUAGE Safe #-}
{-

* Data.Aeson:
  -- Can't be safely imported! The module itself isn't safe

* RemoveFacebookContent.Api:
  -- Can't be safely imported! The module itself isn't safe
* RemoveFacebookContent.Cmd:
  -- Can't be safely imported! The module itself isn't safe
* RemoveFacebookContent.Com:
  -- Can't be safely imported! The module itself isn't safe

-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Prelude hiding (until)
import           Data.Aeson     (decode, eitherDecode)
import           Data.Text      (unpack)

import qualified RemoveFacebookContent.Api as Api
import qualified RemoveFacebookContent.Cmd as Cmd
import qualified RemoveFacebookContent.Com as Com

--------------------------------------------------------------------------------

performRemoval
  :: Cmd.Remove
  -> IO ()

retrievePosts
  :: String
  -> IO (Maybe Com.PostData)

deletePost
  :: String
  -> String
  -- -> IO (Either Error Success)
  -> IO (Either String Com.Error)

deletePosts
  :: String
  -> Cmd.Method
  -> Maybe Com.PostData
  -> IO ()

main
  :: IO ()

--------------------------------------------------------------------------------

performRemoval Cmd.Foo =
  do
    return ()
performRemoval (Cmd.Posts token until method) =
  do
    posts <- retrievePosts $ Api.posts token $ Cmd.toUTCTime until

    deletePosts token method $ posts

retrievePosts url =
  do
    response <- Api.get url
    
    return $ decode response

deletePost token uri =
  do
    response <- Api.delete $ Api.remove token uri
    
    return $ eitherDecode response
    -- return $ decode response

deletePosts
  token
  method
  (Just (Com.PostData posts (Com.PostPaging _ next))) =
  do
    mapM_
      (\(Com.Post _ _ uid ts) ->
         do
           let fbid = unpack uid
           putStrLn $ show ts ++ " | " ++ fbid

           case method of
             Cmd.Tentative ->
               do
                 putStrLn $ "-> Tentative: Doesn't perform any deletion."
             Cmd.Confirmed ->
               do
                 response <- deletePost token $ unpack uid
                 putStrLn $ "-> " ++ show response
      ) posts

    posts' <-retrievePosts $ unpack $ next 

    deletePosts token method $ posts'
deletePosts _ _ Nothing =
  do
    return ()
  
main =
  do
    args <- Cmd.arguments
    performRemoval args
