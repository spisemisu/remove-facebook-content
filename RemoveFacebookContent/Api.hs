-- {-# LANGUAGE Safe #-}
{-

* Network.HTTP.Client:
  -- Can't be safely imported! The module itself isn't safe
* Network.HTTP.Client.TLS:
  -- Can't be safely imported! The module itself isn't safe

-}

--------------------------------------------------------------------------------

module RemoveFacebookContent.Api
  ( delete
  , get
  , me
  , metadata
  , posts
  , remove
  ) where

--------------------------------------------------------------------------------

import           Prelude hiding (until)
import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Time
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS   (tlsManagerSettings)
import           Network.HTTP.Types.Method (methodGet, methodDelete)

--------------------------------------------------------------------------------

get
  :: String -> IO L8.ByteString

delete
  :: String -> IO L8.ByteString

graphUri
  :: String
graphVer
  :: String
protocol
  :: String

query
  :: [ (String, String) ] -> String

me
  :: String -> String
metadata
  :: String -> String
posts
  :: String -> Maybe UTCTime -> String
remove
  :: String -> String -> String


--------------------------------------------------------------------------------

get url = do
  manager  <- newManager   tlsManagerSettings
  request  <- parseRequest url
  response <- httpLbs request { method = methodGet } manager

  return $ responseBody response

delete url = do
  manager  <- newManager   tlsManagerSettings
  request  <- parseRequest url
  response <- httpLbs request { method = methodDelete } manager

  return $ responseBody response

--------------------------------------------------------------------------------

graphUri = "graph.facebook.com"
graphVer = "v2.12"
protocol = "https://"

query [] = []
query xs =
  "?" ++ aux xs
  where
    aux [        ] = []
    aux ((k,v):ys) =
      k ++ "=" ++ v ++ "&" ++ aux ys

me token =
  protocol ++ graphUri ++ "/" ++ graphVer ++ "/me" ++ querystring
  where
    querystring = query [ ("access_token", token) ]

metadata token =
  protocol ++ graphUri ++ "/" ++ graphVer ++ "/me" ++ querystring
  where
    querystring = query [ ("metadata","1"), ("access_token",token) ]

posts token until =
  protocol ++ graphUri ++ "/" ++ graphVer ++ "/me/posts" ++ querystring
  where
    querystring =
      query $ [ ("access_token", token) ] ++ timestamp
      where
        timestamp =
          case until of
            Just ts ->
              [ ("until", iso8601 ts) ]
            Nothing ->
              []

        iso8601 = formatTime defaultTimeLocale "%FT%T%QZ"

remove token uid =
  protocol ++ graphUri ++ "/" ++ graphVer ++ "/" ++ uid ++ querystring
  where
    querystring =
      query $ [ ("access_token", token) ]
  
{-

https://developers.facebook.com/tools/explorer/

https://developers.facebook.com/docs/graph-api/using-graph-api/

-}
