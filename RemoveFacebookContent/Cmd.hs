{-# LANGUAGE DeriveDataTypeable #-}
-- {-# LANGUAGE Safe               #-}
{-

* System.Console.CmdArgs:
  -- Can't be safely imported! The module itself isn't safe
-}

--------------------------------------------------------------------------------

module RemoveFacebookContent.Cmd
  ( Remove (..)
  , Method (..)
  , arguments
  , toUTCTime
  ) where

--------------------------------------------------------------------------------

import           Prelude hiding
  ( until
  )
import           Data.Data
import           Data.Time
import           System.Console.CmdArgs

--------------------------------------------------------------------------------

newtype Timestamp
  = Timestamp String
  deriving (Data, Typeable, Show)
  
data Method
  = Tentative
  | Confirmed
  deriving (Data, Typeable, Show)

data Remove
  = Posts -- /me/posts
    { authToken :: String
    , until     :: Maybe Timestamp
    , method    :: Method
    }
  {- Deleting: You can't perform this operation on this endpoint.
  - developers.facebook.com/docs/graph-api/reference/v2.12/user/likes#Deleting
  - developers.facebook.com/docs/graph-api/reference/v2.12/user/feed#delete
  - developers.facebook.com/docs/graph-api/reference/user/albums#Deleting
  - developers.facebook.com/docs/graph-api/reference/user/photos#Deleting
  - developers.facebook.com/docs/graph-api/reference/user/tagged_places#Deleting
  | Likes        -- /me/likes
  | Feed         -- /me/feed
  | Albums       -- /me/albums
  | Photos       -- /me/photos
  | TaggedPlaces -- /me/tagged_places -}
  | Foo
  deriving (Data, Typeable, Show)

--------------------------------------------------------------------------------

toUTCTime
  :: Maybe Timestamp
  -> Maybe UTCTime

tokenArg
  :: String
  -> String
timestampArg
  :: Maybe Timestamp
  -> Maybe Timestamp

posts
  :: Remove
foos
  :: Remove

arguments
  :: IO Remove

--------------------------------------------------------------------------------

toUTCTime (Just (Timestamp x)) =
  parseTimeM True defaultTimeLocale  "%Y-%m-%dT%H:%M:%S" x
toUTCTime Nothing =
  Nothing

tokenArg x =
  x
  &= help "Mandatory: Token to access Facebooks Graph API."
  &= typ  "AUTHTOKEN"

timestampArg x =
  x
  &= help "Optional: An ISO-8601 date (YYYY-MM-DDTHH:MM:SS)."
  &= typ  "TIMESTAMP"

posts =
  Posts
  { authToken  = tokenArg     def
  , until      = timestampArg def
  , method     =
    enum
    [ Tentative &= help "Doesn't perform any deletion."
    , Confirmed &= help "Does perform the deletions."
    ]
  }
  &= help "Remove post (status updates) from your timeline"

foos =
  Foo
  &= help "Only added to show multiple CmdArg modes"

arguments =
  do
    cmdArgs $ modes [ posts, foos ]
      &= program "./RFC"
      &= summary "Remove Facebook Content v0.11"
