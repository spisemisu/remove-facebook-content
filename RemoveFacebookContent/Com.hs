{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE Safe              #-}
{-

* Data.Aeson:
  -- Can't be safely imported! The module itself isn't safe

-}

--------------------------------------------------------------------------------

module RemoveFacebookContent.Com
  ( Error        (..)
  , ErrorDetails (..)
  , Post         (..)
  , PostPaging   (..)
  , PostData     (..)
  , Success      (..)
  ) where

--------------------------------------------------------------------------------

import           Control.Monad
import           Data.Aeson hiding (Success, Error)
import           Data.Time
import           Data.Text

--------------------------------------------------------------------------------

data ErrorDetails
  = ErrorDetails
    { description :: Text
    , type'       :: Text
    , code        :: Int
    , traceid     :: Text
    }
  deriving Show

data Error
  = Error
    { error :: ErrorDetails
    }
  deriving Show

data Success
  = Success
    { success :: Bool
    }
  deriving Show

data Post
  = Post
    { message     :: Maybe Text
    , story       :: Maybe Text
    , identifier  :: Text
    , createdTime :: UTCTime
    }
  deriving Show

data PostPaging
  = PostPaging
    { previous :: Text
    , next     :: Text
    }
  deriving Show

data PostData
  = PostData
    { posts  :: [ Post ]
    , paging :: PostPaging
    }
  deriving Show
  
--------------------------------------------------------------------------------

instance FromJSON ErrorDetails where
  parseJSON (Object x) =
    ErrorDetails
    <$> x .: "message"
    <*> x .: "type" 
    <*> x .: "code"
    <*> x .: "fbtrace_id"
  parseJSON _ = mzero
  
instance FromJSON Error where
  parseJSON (Object x) =
    Error
    <$> x .: "error"
  parseJSON _ = mzero
  
instance FromJSON Success where
  parseJSON (Object x) =
    Success
    <$> x .: "success"
  parseJSON _ = mzero

instance FromJSON Post where
  parseJSON (Object x) =
    Post
    <$> x .:? "message"
    <*> x .:? "story" 
    <*> x .:  "id"
    <*> x .:  "created_time"
  parseJSON _ = mzero
  
instance FromJSON PostPaging where
  parseJSON (Object x) =
    PostPaging
    <$> x .: "previous"
    <*> x .: "next"
  parseJSON _ = mzero
  
instance FromJSON PostData where
  parseJSON (Object x) =
    PostData
    <$> x .: "data"
    <*> x .: "paging"
  parseJSON _ = mzero

--------------------------------------------------------------------------------

{-

JSON Reponses:

a) for: likes, albums, photos, ...

{ data :
  [ { name         : "Some text ... "
    , id           : "Some unique identifier ... "
    , created_time : "Some ISO-8601 timestamp ... "
    }
  ],
  paging :
  { cursors :
    { before : "MTY4MzA0NzAyNTI2MjA4MQZDZD"
    , after  : "NjQwMDEwMDEyNjg1MzE0"
    }
  , next  : "https://graph.facebook.com/v2.12/ ..."
  }
}

b) for: posts, feed, ...

-- has support for "until" 

{ data :
  [ { message      : "Some text ... "
    , id           : "Some unique identifier ... "
    , created_time : "Some ISO-8601 timestamp ... "
    }
  ],
  paging :
  { previous : "https://graph.facebook.com/v2.12/ ..."
  , next     : "https://graph.facebook.com/v2.12/ ..."
  }
}

-}
