Haskell Script
==============

Haskell Script to Remove Facebook Content that was prepared for the MF#K Meetup:

* (Hands-on Haskell) MF#K: Delete your Facebook account or just remove content?
    - https://www.meetup.com/MoedegruppeFunktionelleKoebenhavnere/events/249159724/ 

### Usage

	./RFC.hs posts \
	-u "2007-12-31T23:59:59" \
	-a "EAACE ... 0gZD"

For more information, type `./RFC --help` to get the following text:
	
	Remove Facebook Content v0.11

	./RFC [COMMAND] ... [OPTIONS]

	Common flags:
      -? --help                 Display help message
      -V --version              Print version information
	     --numeric-version      Print just the version number
		 
	./RFC posts [OPTIONS]
	  Remove post (status updates) from your timeline
		   
	  -a --authtoken=AUTHTOKEN  Mandatory: Token to access Facebooks Graph API.
	  -u --until=TIMESTAMP      Optional: An ISO-8601 date (YYYY-MM-DDTHH:MM:SS).
	  -t --tentative            Doesn't perform any deletion.
	  -c --confirmed            Does perform the deletions.
				   
	./RFC foo [OPTIONS]
	  Only added to show multiple CmdArg modes

### Requirements

In order to run the script, `The Haskell Tool Stack` must be installed:

* https://docs.haskellstack.org/en/stable/install_and_upgrade/
